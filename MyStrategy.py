from model.ActionType import ActionType
from model.VehicleType import VehicleType
from model.Game import Game
from model.Move import Move
from model.Player import Player
from model.World import World

import math
from copy import deepcopy

class MyStrategy:
    vehicles = {}
    me = []
    delayedMove = []
    lastTick = 0;

    def median(self, type=-1, player=-1):
        if player==-1:
            player = self.me
        bounds = self.bounds(type, player)
        return [(bounds[0]+bounds[2])/2, (bounds[1]+bounds[3])/2]

    def density(self, p1, p2,  type=-1, player=-1):
        if player==-1:
            player = self.me
        left = min(p1[0], p2[0])
        right = max(p1[0], p2[0])
        top = min(p1[1], p2[1])
        bottom = max(p1[1], p2[1])
        cnt = len([v for v in self.vehicles.values() if (v.type==type or type<0) and v.player_id==player.id and left<=v.x<=right and top<=v.y<=bottom])
        return cnt
        
    def center(self, deep, bounds, type=-1, player=-1):
        print("bounds", bounds)

        median = [abs(bounds[2]+bounds[0])/2.0, abs(bounds[3]+bounds[1])/2.0]

        if deep==0:
            return median
        
        parts = {}
        topBounds = bounds
        topDensity = -1
        
        for curBounds in [ [[bounds[0], bounds[1]], median], [median, [bounds[2], bounds[3]]], [median, [bounds[0], bounds[3]]], [median, [bounds[2], bounds[1]]] ] :
            curDensity = self.density(curBounds[0], curBounds[1], type, player);
            #print(curBounds, curDensity)
            if curDensity > topDensity:
                topDensity = curDensity
                topBounds = [min(curBounds[0][0],curBounds[1][0]), min(curBounds[0][1], curBounds[1][1]), max(curBounds[0][0],curBounds[1][0]), max(curBounds[0][1], curBounds[1][1])]
        
        return self.center(deep-1, topBounds, type, player)
        

    def bounds(self, type=-1, player=-1):
        if player==-1:
            player = self.me
        xs = [t.x for t in self.vehicles.values() if (t.type==type or type<0) and t.player_id==player.id]
        ys = [t.y for t in self.vehicles.values() if (t.type==type or type<0) and t.player_id==player.id]
        #print([min(xs), min(ys), max(xs), max(ys)])
        return [min(xs), min(ys), max(xs), max(ys)]

    def doSelect(self, move, p1, p2, type=-1):
        move.action = ActionType.CLEAR_AND_SELECT
        move.left = min(p1[0], p2[0])
        move.right = max(p1[0], p2[0])
        move.top = min(p1[1], p2[1])
        move.bottom = max(p1[1], p2[1])
        if type>=0:
            move.vehicle_type = type

    def doMove(self, move, vec):
        move.action = ActionType.MOVE
        move.x, move.y = vec


    def doRotate(self, move, vec, angle):
        print(vec)
        move.action = ActionType.ROTATE
        move.x, move.y = vec
        move.angle = angle

    def doMall(self, move, tickIndex, zipFactor):
        timeToPack = min(math.ceil(zipFactor/0.4), 20)
        zipFactor = timeToPack*0.4
        cycle = timeToPack*6;

        armyMed = self.median()
        armyBounds = self.bounds()

        #print(armyBounds[2] - armyBounds[0])

        if tickIndex % cycle == 1:
            self.doSelect(move, armyMed, [armyBounds[2], armyBounds[3]])

        if tickIndex % cycle == 2:
            self.doMove(move, [-zipFactor,-zipFactor])

        if tickIndex % cycle == timeToPack + 1:
            self.doSelect(move, armyMed, [armyBounds[0], armyBounds[1]])

        if tickIndex % cycle == timeToPack + 2:
            self.doMove(move, [zipFactor,zipFactor])

        if tickIndex % cycle == timeToPack*2 + 1:
            self.doSelect(move, armyMed, [armyBounds[2], armyBounds[1]])

        if tickIndex % cycle == timeToPack*2 + 2:
            self.doMove(move, [-zipFactor,zipFactor])

        if tickIndex % cycle == timeToPack*3 + 2:
            self.doSelect(move, armyMed, [armyBounds[0], armyBounds[3]])

        if tickIndex % cycle == timeToPack*3 + 3:
            self.doMove(move, [zipFactor,-zipFactor])

        if tickIndex % cycle == timeToPack*4 + 1:
            self.doSelect(move, [armyBounds[0], armyBounds[1]], [armyBounds[2], armyBounds[3]], -1)

        if tickIndex % cycle == timeToPack*4 + 2:
            self.doRotate(move, armyMed, -math.pi/4.0)
    
    def relatedDensity(self, type=-1, player=-1):
        bounds = self.bounds(type, player)
        square = abs(bounds[2] - bounds[0])*abs(bounds[3] - bounds[1])
        relD = square/self.density(bounds[0:2], bounds[2:4], type, player) 
        print(relD)
        return relD
        
    def needMall(self):
        return self.relatedDensity() > 24
        
    def move(self, me: Player, world: World, game: Game, move: Move):
        self.me = me

        for v in world.new_vehicles:
            self.vehicles[v.id] = deepcopy(v)

        if world.tick_index > 0:
            for vu in world.vehicle_updates:
                if vu.durability == 0:
                    del self.vehicles[vu.id]
                else:
                    self.vehicles[vu.id].update(vu)
        
        armyMed = self.median()
        armyBounds = self.bounds()
        self.relatedDensity()

        if self.needMall():
            self.doMall(move, world.tick_index, 10)
        else:
            #print((world.tick_index - self.lastTick) % 100)
            if (world.tick_index - self.lastTick) % 100 == 0:
                self.doSelect(move, armyBounds[0:2], armyBounds[2:4], -1)
            if (world.tick_index - self.lastTick) % 100 == 1:
                opponentArmyBounds = self.bounds(-1, world.get_opponent_player())
                opponentArmyCenter = self.center(3, opponentArmyBounds, -1, world.get_opponent_player())
                target = opponentArmyCenter
                #print(target)
                self.doMove(move, [target[0] - armyMed[0], target[1] - armyMed[1]])
                move.max_speed = 0.4

