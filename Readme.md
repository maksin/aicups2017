## О коде
Реализация стратегии для ежегодного соревнования https://russianaicup.ru 2017 года

Результат - попадание в top 250, выход в финал и призовая футболка

Код писался по вечерам и в ритме соревнования, поэтому качество кода - последнее на что стоит смотреть.

Но алгоритм заслуживает внимания.

## About this
This is a realisation of strategy for annual AI contest https://russianaicup.ru by http://mail.ru

Reached top 250 and got prize t-shirt :)